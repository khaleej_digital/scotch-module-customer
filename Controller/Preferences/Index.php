<?php
declare(strict_types=1);

namespace Beside\Customer\Controller\Preferences;

use Beside\Customer\Controller\BaseAccountMenuController;
use Magento\Framework\App\ActionInterface;

/**
 * Class Index
 *
 * @package Beside\Customer\Controller\Preferences
 */
class Index extends BaseAccountMenuController implements ActionInterface
{
}

