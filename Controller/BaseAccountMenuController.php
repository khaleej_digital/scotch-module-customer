<?php
declare(strict_types=1);

namespace Beside\Customer\Controller;

use Magento\Customer\Model\Session;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\ActionInterface;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\Controller\Result\RedirectFactory;

/**
 * Class BaseAccountMenuController
 *
 * @package Beside\Customer\Controller
 */
abstract class BaseAccountMenuController implements ActionInterface
{
    /** @var Session */
    protected $customerSession;

    /** @var PageFactory */
    protected $resultPageFactory;

    /** @var RedirectFactory */
    protected $redirectFactory;

    /**
     * Index constructor.
     *
     * @param PageFactory $resultPageFactory
     * @param RedirectFactory $redirectFactory
     * @param Session $customerSession
     */
    public function __construct(
        PageFactory $resultPageFactory,
        RedirectFactory $redirectFactory,
        Session $customerSession
    ) {
        $this->customerSession = $customerSession;
        $this->redirectFactory = $redirectFactory;
        $this->resultPageFactory = $resultPageFactory;
    }

    /**
     * Execute method
     *
     * @return ResponseInterface|ResultInterface
     */
    public function execute() {

        if (!$this->customerSession->isLoggedIn()) {
            $resultRedirect = $this->redirectFactory->create();
            return $resultRedirect->setPath('customer/account/login');
        }

        return $this->resultPageFactory->create();
    }
}
