<?php
declare(strict_types=1);

namespace Beside\Customer\Controller\Promotions;

use Beside\Customer\Controller\BaseAccountMenuController;
use Magento\Framework\App\ActionInterface;

/**
 * Class Index
 *
 * @package Beside\Customer\Controller\Promotions
 */
class Index extends BaseAccountMenuController implements ActionInterface
{
}
