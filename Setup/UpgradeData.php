<?php
/**
 * @category  Beside
 * @package   Beside_Customer
 * @author    Emrah Uyanik <emrah.uyanik@redboxdigital.com>
 * @copyright Copyright (c) 2021 Redbox Digital (http://www.redboxdigital.com)
 */

namespace Beside\Customer\Setup;

use Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface;
use Magento\Eav\Model\Entity\Attribute\Source\Boolean;
use Magento\Eav\Setup\EavSetupFactory;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\UpgradeDataInterface;

/**
 * Class UpgradeData
 * @package Beside\Customer\Setup
 */
class UpgradeData implements \Magento\Framework\Setup\UpgradeDataInterface
{
    /**
     * @var EavSetupFactory
     */
    private $eavSetupFactory;

    /**
     * UpgradeData constructor.
     * @param EavSetupFactory $eavSetupFactory
     */
    public function __construct(
        \Magento\Eav\Setup\EavSetupFactory $eavSetupFactory
    ) {
        $this->eavSetupFactory = $eavSetupFactory;
    }

    /**
     * @param ModuleDataSetupInterface $setup
     * @param ModuleContextInterface $context
     */
    public function upgrade(
        \Magento\Framework\Setup\ModuleDataSetupInterface $setup,
        \Magento\Framework\Setup\ModuleContextInterface $context
    ) {
        if (version_compare($context->getVersion(), '0.1.0', '<')) {
            $this->updateAttributes();
        }
    }

    /**
     * Update rma item attributes
     */
    public function updateAttributes()
    {
        $eavSetup = $this->eavSetupFactory->create();
        $eavSetup->updateAttribute(
            'customer_address',
            'telephone',
            'validate_rules',
            '{"min_text_length":"12","max_text_length":"15","input_validation":"length"}'
        );
    }
}
