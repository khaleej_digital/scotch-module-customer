<?php
declare(strict_types=1);

namespace Beside\Customer\Setup\Patch\Data;

use Magento\Customer\Api\AddressMetadataManagementInterface as AddressInterface;
use Magento\Eav\Setup\EavSetupFactory;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\Patch\DataPatchInterface;
use Magento\Customer\Setup\CustomerSetupFactory;
use Magento\Customer\Setup\CustomerSetup;
use Magento\Eav\Model\Entity\Attribute\SetFactory as AttributeSetFactory;
use Magento\Customer\Model\ResourceModel\Attribute as ResourceModel;
use Magento\Eav\Model\Entity\Attribute\Set as AttributeSet;

/**
 * Class AddTelephonePrefixAttribute
 *
 * @package Beside\Customer\Setup\Patch\Data
 */
class AddTelephonePrefixAttribute implements DataPatchInterface
{
    /**
     * Customer address attribute Mobile telephone prefix code
     */
    public const TELEPHONE_PREFIX_ATTRIBUTE = 'customer_mobile_number_prefix';

    /**
     * @var CustomerSetupFactory
     */
    private $customerSetupFactory;

    /**
     * @var ModuleDataSetupInterface
     */
    private $setup;

    /**
     * @var AttributeSetFactory
     */
    private AttributeSetFactory $attributeSetFactory;

    /**
     * @var ResourceModel
     */
    private ResourceModel $resourceModel;

    /**
     * SetTelephoneOptional constructor.
     *
     * @param CustomerSetupFactory $customerSetupFactory
     * @param ModuleDataSetupInterface $setup
     * @param AttributeSetFactory $attributeSetFactory
     * @param ResourceModel $resourceModel
     */
    public function __construct(
        CustomerSetupFactory $customerSetupFactory,
        ModuleDataSetupInterface $setup,
        AttributeSetFactory $attributeSetFactory,
        ResourceModel $resourceModel
    ) {
        $this->customerSetupFactory = $customerSetupFactory;
        $this->setup = $setup;
        $this->attributeSetFactory = $attributeSetFactory;
        $this->resourceModel = $resourceModel;
    }

    /**
     * Get dependencies
     *
     * @return array
     */
    public static function getDependencies(): array
    {
        return [];
    }

    /**
     * Add customer address attribute - mobile telephone prefix
     *
     * @return $this
     * @throws \Magento\Framework\Exception\AlreadyExistsException
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Zend_Validate_Exception
     */
    public function apply(): self
    {
        /** @var CustomerSetup $customerSetup */
        $customerSetup = $this->customerSetupFactory->create(['setup' => $this->setup]);
        $entityAttribute = [
            'type' => 'varchar',
            'label' => 'Mobile Telephone Prefix',
            'input' => 'text',
            'required' => true,
            'position' => 125,
            'sort_order' => 125,
            'visible' => true,
            'system' => false,
            'visible_on_front' => true,
            'user_defined' => true,
        ];

        $customerSetup->addAttribute(
            AddressInterface::ENTITY_TYPE_ADDRESS,
            self::TELEPHONE_PREFIX_ATTRIBUTE,
            $entityAttribute
        );
        $attribute = $customerSetup->getEavConfig()->getAttribute(
            AddressInterface::ENTITY_TYPE_ADDRESS,
            self::TELEPHONE_PREFIX_ATTRIBUTE
        );
        $customerAddressEntity = $customerSetup->getEavConfig()->getEntityType(AddressInterface::ENTITY_TYPE_ADDRESS);
        $attributeSetId = $customerAddressEntity->getDefaultAttributeSetId();
        /** @var $attributeSet AttributeSet */
        $attributeSet = $this->attributeSetFactory->create();
        $attributeGroupId = $attributeSet->getDefaultGroupId($attributeSetId);

        $data = [
            'attribute_set_id' => $attributeSetId,
            'attribute_group_id' => $attributeGroupId,
            'used_in_forms' => ['adminhtml_customer_address','customer_address_edit','customer_register_address'],
        ];

        $attribute->addData($data);
        $this->resourceModel->save($attribute);

        return $this;
    }

    /**
     * Get aliases
     *
     * @return array
     */
    public function getAliases(): array
    {
        return [];
    }
}
