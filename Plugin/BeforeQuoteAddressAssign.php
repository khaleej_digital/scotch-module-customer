<?php
declare(strict_types=1);

namespace Beside\Customer\Plugin;

/**
 * Class BeforeQuoteAddressAssign
 *
 * @package Beside\Customer\Plugin
 */
class BeforeQuoteAddressAssign
{
    /**
     * Set customer mobile number prefix
     *
     * @param \Magento\Quote\Model\ShippingAddressManagement $subject
     * @param $cartId
     * @param \Magento\Quote\Api\Data\AddressInterface $address
     */
    public function beforeAssign(
        \Magento\Quote\Model\ShippingAddressManagement $subject,
        $cartId,
        \Magento\Quote\Api\Data\AddressInterface $address
    ) {
        $mobilePrefix = $address->getCustomAttribute('customer_mobile_number_prefix');
        if ($mobilePrefix) {
            $value = $mobilePrefix->getValue();
            $mobilePrefixValue = $value['value'] ?? '';
            $address->setCustomAttribute('customer_mobile_number_prefix', $mobilePrefixValue);
        }
    }
}
